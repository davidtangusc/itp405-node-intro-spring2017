var express = require('express');
var mysql = require('mysql');
var app = express();

app.get('/songs/:id', function (request, response) {
  var connection = mysql.createConnection({
    host     : 'itp460.usc.edu',
    user     : 'student',
    password : 'ttrojan',
    database : 'music'
  });

  connection.connect();
  connection.query('SELECT * FROM songs WHERE id = ?', [ request.params.id ], function(error, songs) {
    if (error) {
      throw error;
    }

    var song = songs[0];

    connection.query('SELECT * FROM artists WHERE id = ?', [ song.artist_id ], function(error, artists) {
      if (error) {
        throw error;
      }

      var artist = artists[0];
      song.artist = artist;
      delete song.artist_id;
      response.json(song);

      connection.end();
    });
  });
});

app.listen(8000);
